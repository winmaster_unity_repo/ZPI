# Introduction 
Project made with my team mates during university course "Team IT venture" (polish: ZPI - Zespołowe przedsięwzięcie informatyczne).

## Description of main idea
Based on VR technology and HTC Vive headset with Steam VR plugin. 
Main goal was to improve memorizing methods with use of 'Mind Palace' technique. We made VR room full of items connected to cusine and furnitures. One room wall was a recipe board. User can pick up suitable groceriess and with lots of creativity arrange them in different positions to move grey matter of brain and better memorize the culinary recipies.


# Screens
![](screens/screen%20(1).png)
![](screens/screen%20(2).png)
![](screens/screen%20(3).png)

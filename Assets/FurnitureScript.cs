﻿using System.Collections.Generic;
using UnityEngine;

public class FurnitureScript : MonoBehaviour
{
    private const string CloneableFurnitureTag = "cloneableFurniture";
    private const string PickupableFurnitureTag = "pickupableFurniture";
    private const string UntaggedTag = "Untagged";

    // Niezmienne
    private FixedJoint FixedJoint;
    private const Valve.VR.EVRButtonId GripButton = Valve.VR.EVRButtonId.k_EButton_Grip;
    private const Valve.VR.EVRButtonId TriggerButton = Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger;
    private readonly List<string> SupportedTagsList = new List<string>()
    {
        CloneableFurnitureTag,
        PickupableFurnitureTag
    };
    private SteamVR_TrackedObject TrackedObject;

    // Zmienne
    private GameObject Object;
    private string ObjectTag;

    private SteamVR_Controller.Device Controller { get { return SteamVR_Controller.Input((int)TrackedObject.index); } }

    void Start()
    {
        TrackedObject = GetComponent<SteamVR_TrackedObject>();
        FixedJoint = GetComponent<FixedJoint>();
    }

    void Update()
    {
        if (Controller == null)
            return;

        if (Controller.GetPressDown(TriggerButton))
            PickUpObj();

        if (Controller.GetPressUp(TriggerButton))
            DropObj();

        if (Controller.GetPressDown(GripButton))
            DeleteObj();
    }

    void OnTriggerStay(Collider collider)
    {
        if (SupportedTagsList.Contains(collider.tag))
        {
            Object = collider.gameObject;
            ObjectTag = Object.tag;
        }
    }

    void OnTriggerExit(Collider collider)
    {
        Object = null;
        ObjectTag = null;
    }

    void PickUpObj()
    {
        if (Object != null)
        {
            if (ObjectTag == PickupableFurnitureTag)
                FixedJoint.connectedBody = Object.GetComponent<Rigidbody>();
            else if (ObjectTag == CloneableFurnitureTag)
            {
                var original = Object.GetComponent<Rigidbody>();
                Rigidbody clone = Instantiate(original, transform.position, transform.rotation) as Rigidbody;

                if (ObjectTag == CloneableFurnitureTag)
                {
                    clone.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                    clone.transform.Rotate(new Vector3(-90.0f, 0.0f, 0.0f));
                }

                clone.tag = ObjectTag == CloneableFurnitureTag
                    ? PickupableFurnitureTag
                    : UntaggedTag;

                clone.constraints = RigidbodyConstraints.None;
                clone.freezeRotation = false;

                clone.GetComponent<Collider>().enabled = false;
                FixedJoint.connectedBody = clone;
            }
        }
        else
            FixedJoint.connectedBody = null;
    }

    void DropObj()
    {
        if (FixedJoint.connectedBody != null)
        {
            FixedJoint.connectedBody.GetComponent<Collider>().enabled = true;
            FixedJoint.connectedBody = null;
        }
    }

    void DeleteObj()
    {
        if (FixedJoint.connectedBody != null)
        {
            FixedJoint.connectedBody = null;
            Destroy(Object);
        }
    }
}

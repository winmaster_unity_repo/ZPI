﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(SteamVR_LaserPointer))]
public class VRUIInput : MonoBehaviour
{
    private SteamVR_LaserPointer laserPointer_;
    private SteamVR_TrackedController trackedController_;

    private void OnEnable()
    {
        laserPointer_ = GetComponent<SteamVR_LaserPointer>();
        laserPointer_.PointerIn += HandlePointerIn;
        laserPointer_.PointerOut += HandlePointerOut;

        trackedController_ =
            GetComponent<SteamVR_TrackedController>()
            ?? GetComponentInParent<SteamVR_TrackedController>();

        trackedController_.TriggerClicked += HandleTriggerClicked;
    }

    private void HandleTriggerClicked(object _sender, ClickedEventArgs _args)
    {
        var gameObject = EventSystem.current.currentSelectedGameObject;

        if (gameObject != null)
            ExecuteEvents.Execute(gameObject, new PointerEventData(EventSystem.current), ExecuteEvents.submitHandler);
    }

    private void HandlePointerIn(object _sender, PointerEventArgs _args)
    {
        var button = _args.target.GetComponent<Button>();
        if (button != null)
        {
            button.Select();
            Debug.Log("HandlerPointerIn", _args.target.gameObject);
        }
    }

    private void HandlePointerOut(object _sender, PointerEventArgs _args)
    {
        var button = _args.target.GetComponent<Button>();
        if (button != null)
        {
            EventSystem.current.SetSelectedGameObject(null);
            Debug.Log("HandlerPointerOut", _args.target.gameObject);
        }
    }
}
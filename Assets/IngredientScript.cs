﻿using System.Collections.Generic;
using UnityEngine;

public class IngredientScript : MonoBehaviour
{
    private const string CloneableTag = "cloneable";
    private const string PickupableTag = "pickupable";
    private const string UntaggedTag = "Untagged";

    // Niezmienne
    private FixedJoint FixedJoint;
    private const Valve.VR.EVRButtonId GripButton = Valve.VR.EVRButtonId.k_EButton_Grip;
    private const Valve.VR.EVRButtonId TriggerButton = Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger;
    private readonly List<string> SupportedTagsList = new List<string>()
    {
        CloneableTag,
        PickupableTag
    };
    private SteamVR_TrackedObject TrackedObject;

    // Zmienne
    private GameObject Object;
    private string ObjectTag;

    private SteamVR_Controller.Device Controller { get { return SteamVR_Controller.Input((int)TrackedObject.index); } }

    void Start()
    {
        TrackedObject = GetComponent<SteamVR_TrackedObject>();
        FixedJoint = GetComponent<FixedJoint>();
    }

    void Update()
    {
        if (Controller == null)
            return;

        if (Controller.GetPressDown(TriggerButton))
            PickUpObj();

        if (Controller.GetPressUp(TriggerButton))
            DropObj();

        if (Controller.GetPressDown(GripButton))
            DeleteObj();
    }

    void OnTriggerStay(Collider collider)
    {
        if (SupportedTagsList.Contains(collider.tag))
        {
            Object = collider.gameObject;
            ObjectTag = Object.tag;
        }
    }

    void OnTriggerExit(Collider collider)
    {
        Object = null;
        ObjectTag = null;
    }

    void PickUpObj()
    {
        if (Object != null)
        {
            if (ObjectTag == PickupableTag)
                FixedJoint.connectedBody = Object.GetComponent<Rigidbody>();
            else if (ObjectTag == CloneableTag)
            {
                var original = Object.GetComponent<Rigidbody>();
                Rigidbody clone = Instantiate(original, transform.position, transform.rotation) as Rigidbody;

                clone.tag = ObjectTag == CloneableTag
                    ? PickupableTag
                    : UntaggedTag;

                clone.constraints = RigidbodyConstraints.None;
                clone.freezeRotation = false;

                clone.GetComponent<Collider>().enabled = false;
                FixedJoint.connectedBody = clone;
            }
        }
        else
            FixedJoint.connectedBody = null;
    }

    void DropObj()
    {
        if (FixedJoint.connectedBody != null)
        {
            FixedJoint.connectedBody.GetComponent<Collider>().enabled = true;
            FixedJoint.connectedBody = null;
        }
    }

    void DeleteObj()
    {
        if (FixedJoint.connectedBody != null)
        {
            FixedJoint.connectedBody = null;
            Destroy(Object);
        }
    }
}

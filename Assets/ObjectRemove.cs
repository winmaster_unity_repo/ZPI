﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRemove : MonoBehaviour 
{
	private Valve.VR.EVRButtonId gripButton = Valve.VR.EVRButtonId.k_EButton_Grip;

	private SteamVR_Controller.Device controller { get { return SteamVR_Controller.Input((int)trackedObj.index); } }
	private SteamVR_TrackedObject trackedObj;

	private GameObject obj;
	private FixedJoint fJoint;

	void Start()
	{
		trackedObj = GetComponent<SteamVR_TrackedObject>();
		fJoint = GetComponent<FixedJoint>();
	}

	void Update()
	{
		if (controller == null)
		{
			Debug.Log("Controller not initialized");
			return;
		}

		var device = SteamVR_Controller.Input((int)trackedObj.index);

		if (controller.GetPressDown(gripButton))
		{
			DropObj();
		}
	}

    void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("pickupable"))
        {
            obj = other.gameObject;
        }
    }

    void OnTriggerExit(Collider other)
    {
        obj = null;
    }

    void DropObj()
	{
		if (fJoint.connectedBody != null)
		{
			Destroy(trackedObj);
		}
	}
}

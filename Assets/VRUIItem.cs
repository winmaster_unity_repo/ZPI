﻿using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class VRUIItem : MonoBehaviour
{
    private void OnEnable()
    {
        ValidateCollider();
    }

    private void OnValidate()
    {
        ValidateCollider();
    }

    private void ValidateCollider()
    {
        var transform = GetComponent<RectTransform>();
        var collider =
            GetComponent<BoxCollider>()
            ?? gameObject.AddComponent<BoxCollider>();

        collider.size = transform.sizeDelta;
    }
}